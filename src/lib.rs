#![no_std]

extern crate cast_trait;
extern crate num_traits;
extern crate vec2;

mod cubic;
mod linear;
mod n;
mod quadratic;

pub use cubic::cubic;
pub use linear::linear;
pub use n::n;
pub use quadratic::quadratic;
