use core::ops::{AddAssign, Mul, MulAssign, Sub};

use cast_trait::Cast;
use num_traits::{One, Zero};
use vec2;

#[inline]
pub fn linear<'out, T, F>(
    out: &'out mut [T; 2],
    p0: &[T; 2],
    p1: &[T; 2],
    t: &F,
) -> &'out mut [T; 2]
where
    T: Clone + Cast<F>,
    for<'a> T: AddAssign<&'a T>,
    for<'a, 'b> &'a T: Sub<&'b T, Output = T>,
    F: Clone + One + Zero + PartialOrd + Cast<T> + Sub<F, Output = F> + Mul<F, Output = F>,
    for<'a> F: MulAssign<&'a F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if t <= &F::zero() {
        *out = p0.clone();
        out
    } else if t >= &F::one() {
        *out = p1.clone();
        out
    } else {
        vec2::lerp::<T, F>(out, p0, p1, t)
    }
}
#[test]
fn test_linear() {
    assert_eq!(linear(&mut [0, 0], &[0, 0], &[200, 0], &0.25), &[50, 0]);
    assert_eq!(linear(&mut [0, 0], &[0, 0], &[200, 0], &0.5), &[100, 0]);
    assert_eq!(linear(&mut [0, 0], &[0, 0], &[200, 0], &0.75), &[150, 0]);
}
