use core::ops::{AddAssign, Mul, MulAssign, Sub};

use cast_trait::Cast;
use num_traits::{FromPrimitive, One, Zero};
use vec2;

use cubic::cubic;
use linear::linear;
use quadratic::quadratic;

#[inline]
pub fn n<'out, T, F>(out: &'out mut [T; 2], points: &[[T; 2]], t: &F) -> &'out mut [T; 2]
where
    T: Clone + Cast<F> + Zero,
    for<'a> T: AddAssign<&'a T>,
    for<'a, 'b> &'a T: Sub<&'b T, Output = T>,
    F: Clone
        + One
        + Zero
        + FromPrimitive
        + PartialOrd
        + Cast<T>
        + Sub<F, Output = F>
        + Mul<F, Output = F>,
    for<'a> F: MulAssign<&'a F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if t <= &F::zero() {
        match points.len() {
            0 => vec2::set_zero(out),
            _ => {
                *out = points[0].clone();
                out
            }
        }
    } else if t >= &F::one() {
        match points.len() {
            0 => vec2::set_zero(out),
            n => {
                *out = points[n - 1].clone();
                out
            }
        }
    } else {
        match points.len() {
            0 => vec2::set_zero(out),
            1 => {
                *out = points[0].clone();
                out
            }
            2 => linear(out, &points[0], &points[1], t),
            3 => cubic(out, &points[0], &points[1], &points[2], t),
            4 => quadratic(out, &points[0], &points[1], &points[2], &points[3], t),
            n => {
                *out = casteljau(points, n - 1, 0, t);
                out
            }
        }
    }
}

#[inline]
fn casteljau<T, F>(points: &[[T; 2]], i: usize, j: usize, t: &F) -> [T; 2]
where
    T: Clone + Cast<F>,
    F: Clone + One + Zero + PartialOrd + Cast<T> + Sub<F, Output = F> + Mul<F, Output = F>,
    for<'a, 'b> &'a F: Sub<&'b F, Output = F> + Mul<&'b F, Output = F>,
{
    if i == 0_usize {
        points[j].clone()
    } else {
        let p0 = casteljau(points, i - 1, j, t);
        let p1 = casteljau(points, i - 1, j + 1, t);

        let p0x = p0[0].clone().cast();
        let p0y = p0[1].clone().cast();
        let p1x = p1[0].clone().cast();
        let p1y = p1[1].clone().cast();

        vec2::new(
            ((F::one() - t.clone()) * p0x + t.clone() * p1x).cast(),
            ((F::one() - t.clone()) * p0y + t.clone() * p1y).cast(),
        )
    }
}

#[test]
fn test_n() {
    assert_eq!(n(&mut [0, 0], &[], &0), &[0, 0]);
    assert_eq!(n(&mut [0, 0], &[], &0.5), &[0, 0]);
    assert_eq!(n(&mut [0, 0], &[], &1), &[0, 0]);

    assert_eq!(
        n(
            &mut [0, 0],
            &[[0, 0], [0, 200], [200, 200], [200, 0]],
            &0.25
        ),
        &[31, 112]
    );
    assert_eq!(
        n(&mut [0, 0], &[[0, 0], [0, 200], [200, 200], [200, 0]], &0.5),
        &[100, 150]
    );
    assert_eq!(
        n(
            &mut [0, 0],
            &[[0, 0], [0, 200], [200, 200], [200, 0]],
            &0.75
        ),
        &[168, 112]
    );

    assert_eq!(
        n(
            &mut [0, 0],
            &[[0, 0], [0, 200], [200, 200], [200, 0], [100, 0]],
            &0.25
        ),
        &[50, 126]
    );
    assert_eq!(
        n(
            &mut [0, 0],
            &[[0, 0], [0, 200], [200, 200], [200, 0], [100, 0]],
            &0.5
        ),
        &[131, 125]
    );
    assert_eq!(
        n(
            &mut [0, 0],
            &[[0, 0], [0, 200], [200, 200], [200, 0], [100, 0]],
            &0.75
        ),
        &[157, 50]
    );
}
